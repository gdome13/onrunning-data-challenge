SELECT
   itemid,
   locationid,
   balancedate,
   AVG(quantity) OVER(PARTITION BY itemid, locationid ORDER BY balancedate ROWS BETWEEN 29 PRECEDING AND CURRENT ROW) AS avg_balance
FROM
   inventory_balance
;
