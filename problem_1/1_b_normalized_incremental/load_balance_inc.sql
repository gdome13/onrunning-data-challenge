BEGIN
;
INSERT INTO
   d_item 
   SELECT DISTINCT
      itemid 
   FROM
      inventory_movement 
   WHERE
      transactiondate = '2020-05-26' 
      ON CONFLICT DO NOTHING;
INSERT INTO
   d_location 
   SELECT DISTINCT
      locationid 
   FROM
      inventory_movement 
   WHERE
      transactiondate = '2020-05-26' 
      ON CONFLICT DO nothing;
INSERT INTO
   d_date 
VALUES
   (
      '2020-05-26' 
   )
   ON CONFLICT DO nothing;
DELETE
FROM
   inventory_balance 
WHERE
   balancedate = '2020-05-26';
INSERT INTO
   inventory_balance 
   SELECT
      d_item.itemid,
      d_location.locationid,
      d_date.calendardate,
      0 
   FROM
      d_date 
      CROSS JOIN
         d_location 
      CROSS JOIN
         d_item 
   WHERE
      d_date.calendardate < '2020-05-26' 
      ON conflict DO nothing;
INSERT INTO
   inventory_balance 
   SELECT
      d_item.itemid,
      d_location.locationid,
      '2020-05-26',
      inventory_balance.quantity + COALESCE(inventory_movement.transferquantity, 0) 
   FROM
      d_location 
      CROSS JOIN
         d_item 
      LEFT JOIN
         inventory_movement 
         ON d_item.itemid = inventory_movement.itemid 
         AND d_location.locationid = inventory_movement.locationid 
         AND inventory_movement.transactiondate = '2020-05-26' 
      LEFT JOIN
         inventory_balance 
         ON d_item.itemid = inventory_balance.itemid 
         AND d_location.locationid = inventory_balance.locationid 
         AND inventory_balance.balancedate = '2020-05-26'::DATE - 1;
COMMIT;
