BEGIN
;
DELETE
FROM
   inventory_balance;
DELETE
FROM
   d_item;
DELETE
FROM
   d_location;
DELETE
FROM
   d_date;
INSERT INTO
   d_item 
   SELECT DISTINCT
      itemid 
   FROM
      inventory_movement 
   WHERE
      transactiondate < '2020-05-26';
INSERT INTO
   d_location 
   SELECT DISTINCT
      locationid 
   FROM
      inventory_movement 
   WHERE
      transactiondate < '2020-05-26';
INSERT INTO
   d_date WITH range_values AS 
   (
      SELECT
         MIN(transactiondate) AS mindate,
         MAX(transactiondate) AS maxdate 
      FROM
         inventory_movement 
   )
   SELECT
      generate_series( mindate::DATE, maxdate::DATE, '1 day' )::DATE AS calendardate 
   FROM
      range_values;
INSERT INTO
   inventory_balance 
   SELECT
      d_item.itemid,
      d_location.locationid,
      d_date.calendardate,
      SUM(COALESCE(transferquantity, 0.0)) OVER (PARTITION BY d_item.itemid, d_location.locationid 
   ORDER BY
      d_date.calendardate ROWS BETWEEN unbounded preceding AND CURRENT ROW) AS quantity 
   FROM
      d_date 
      CROSS JOIN
         d_location 
      CROSS JOIN
         d_item 
      LEFT JOIN
         inventory_movement im 
         ON d_date.calendardate = im.transactiondate 
         AND d_location.locationid = im.locationid 
         AND d_item.itemid = im.itemid 
   WHERE
      calendardate < '2020-05-26';
COMMIT;
