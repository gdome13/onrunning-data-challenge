BEGIN
;
DELETE
FROM
   inventory_balance;
INSERT INTO
   inventory_balance WITH range_values AS 
   (
      SELECT
         MIN(transactiondate) AS mindate,
         MAX(transactiondate) AS maxdate 
      FROM
         inventory_movement 
   )
,
   dates AS 
   (
      SELECT
         generate_series( mindate::DATE, maxdate::DATE, '1 day' ) AS calendar_date 
      FROM
         range_values 
   )
,
   locations AS 
   (
      SELECT DISTINCT
         locationid AS locationid 
      FROM
         inventory_movement 
   )
,
   items AS 
   (
      SELECT DISTINCT
         itemid AS itemid 
      FROM
         inventory_movement 
   )
   SELECT
      items.itemid,
      locations.locationid,
      dates.calendar_date,
      SUM(COALESCE(transferquantity, 0.0)) OVER (PARTITION BY items.itemid, locations.locationid 
   ORDER BY
      dates.calendar_date ROWS BETWEEN unbounded preceding AND CURRENT ROW) AS quantity 
   FROM
      dates 
      CROSS JOIN
         locations 
      CROSS JOIN
         items 
      LEFT JOIN
         inventory_movement im 
         ON dates.calendar_date = im.transactiondate 
         AND locations.locationid = im.locationid 
         AND items.itemid = im.itemid 
;	
COMMIT;
