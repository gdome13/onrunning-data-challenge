CREATE TABLE IF NOT EXISTS inventory_movement (
  ItemId VARCHAR(255),
  LocationId VARCHAR(255),
  TransactionDate DATE,
  TransferQuantity NUMERIC
);

TRUNCATE TABLE inventory_movement;
COPY inventory_movement(ItemId, LocationId, TransactionDate, TransferQuantity)
FROM '/home/gdome/personal/hobbydev/onrunning/data_eng_challenge/data/problem_1/inventory_movement.csv'
DELIMITER ','
CSV HEADER;
