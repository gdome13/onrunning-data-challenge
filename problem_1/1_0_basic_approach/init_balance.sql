CREATE TABLE IF NOT EXISTS inventory_balance (
  ItemId VARCHAR(255),
  LocationId VARCHAR(255),
  BalanceDate DATE,
  Quantity NUMERIC,
  PRIMARY KEY(ItemId, LocationId,BalanceDate)
);
