SELECT
   * 
FROM
   inventory_balance 
WHERE
   balancedate = 
   (
      SELECT
         MAX(balancedate) 
      FROM
         inventory_balance 
   )
