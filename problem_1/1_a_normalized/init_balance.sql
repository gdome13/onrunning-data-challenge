CREATE TABLE IF NOT EXISTS inventory_balance (
  ItemId VARCHAR(255) references d_item(ItemId),
  LocationId VARCHAR(255) references d_location(LocationId),
  BalanceDate DATE references d_date(CalendarDate),
  Quantity NUMERIC,
  PRIMARY KEY(ItemId, LocationId,BalanceDate)
);

