"""
This module parses the table and field names from a simple sql script.
"""
import re

from typing import Tuple, List


CLAUSE_REGEX = re.compile(r"^select\s+|\s+select\s|\s+from\s+|\s+where\s+", re.IGNORECASE)
COMMENT_REGEX = re.compile(r"^\s*--.*?\n", re.MULTILINE)
IDENTIFIER_REGEX = r"^\w+$"
DBT_BINARY_FUNCTION_REGEX = r"^{{\s*\w+\s*\(s*'\w+'\s*,\s*'(\w+)'\s*\)\s+}}$"
DBT_UNARY_FUNCTION_REGEX = r"{{\s*\w+\s*\(\s*'(\w+)'\s*\)\s*}}"
FUNCTION_REGEX = r"\w+.*\(\s*\).*"
AS_REGEX = re.compile(r"\s+as\s+", re.IGNORECASE)


def extract_main_parts(sql: str) -> Tuple[str, str]:
    """
    Extract select and from clauses from sql
    :param sql: sql to be parsed
    :return: (from_clause, select_clause)
    """
    a = re.split(CLAUSE_REGEX, sql)
    return a[1], a[2]


def extract_table_name(from_clause: str) -> str:
    """
    Extract table name from a from clause
    :param from_clause: part of query after the from keyword
    :return: table name
    """
    string = from_clause.strip()
    m = re.match(IDENTIFIER_REGEX, string)
    if m:
        return string
    m = re.match(DBT_BINARY_FUNCTION_REGEX, string)
    if m:
        return m.group(1)
    raise Exception('Table name could not be extracted from: {}'.format(string))


def extract_fieldname(string: str) -> str:
    """
    Extract fieldname from a field expressiom in the select clause
    :param string: field expressiom
    :return: field name
    """
    # split on AS
    a = re.split(AS_REGEX, string)
    s = a[0]
    if s == '':
        return None
    # function
    m = re.match(FUNCTION_REGEX, s)
    if m:
        return None
    # actual fieldname
    m = re.match(IDENTIFIER_REGEX, s)
    if m:
        return s
    # unary dbt template function
    m = re.match(DBT_UNARY_FUNCTION_REGEX, a[0])
    if m:
        return m.group(1)
    else:
        raise Exception('Field name could not be extracted from: {}'.format(string))


def extract_fields(select_clause: str) -> List[str]:
    """
    Extract all field names from the select clause
    :param select_clause: part of the query between select and from keywords
    :return:list of fieldnames
    """
    fields = []
    for expr in select_clause.split(','):
        s = expr.strip()
        fieldname = extract_fieldname(s)
        if fieldname is not None:
            fields.append(fieldname)
    return fields


def remove_comments_and_replace_whitespaces(sql: str) -> str:
    """
    Remove comments and replace all whitespaces to single space in sql query
    :param sql: sql query to do the operation on
    :return: string fixed
    """
    sql = re.sub(COMMENT_REGEX, "", sql)
    sql = ' '.join(sql.split())
    return sql


def parse(sql: str) -> dict:
    """
    Parse table name and all fieldnames from sql
    :param sql: sql to parse
    :return: dict cotaing table and fieldnames
    """
    table_data = {}
    sql = remove_comments_and_replace_whitespaces(sql)
    select_clause, from_clause = extract_main_parts(sql)
    table_data['table'] = extract_table_name(from_clause)
    table_data['fields'] = extract_fields(select_clause)
    return table_data
