from unittest import TestCase

import simplesqlparser


class TestSimpleSqlParser(TestCase):
    def setUp(self):
        self.sql_simple = "select a,b from c"
        self.sql_simple_where = "SELECT a,b from c where d = 8"
        self.sql_keyword_as_substring = "select selector,b from c where = 8"
        self.sql_comment = """
select aa as a,
      -- lookups
      bb as b
from c      
        """
        self.from_clause_simple = 'c'
        self.from_clause_dbt = "{{ source('financialforce', 'c_2_g_coda_year_c') }}"
        self.fieldname_simple = "bb AS b"
        self.fieldname_dbt = "{{ null_if ('id') }} as year_id"
        self.fieldname_function = "timestamp(current_date()) as snapshot_date"
        self.fieldname_empty = ""
        self.sql_complex = """
select {{ null_if ('id') }} as year_id
      ,{{ null_if ('name') }} as year_name
      ,{{ null_if ('c_2_g_status_c') }} as year_is_closed
      ,c_2_g_start_date_c as year_start_date
      ,c_2_g_end_date_c as year_end_date
      -- lookups
      ,{{ null_if ('c_2_g_owner_company_c') }} as year_owner_company
from {{ source('financialforce', 'c_2_g_coda_year_c') }}
where is_deleted = FALSE        
        """


class TestRemoveComments(TestSimpleSqlParser):
    def test_remove_comments(self):
        sql = simplesqlparser.remove_comments_and_replace_whitespaces(self.sql_comment)
        expected = 'select aa as a, bb as b from c'
        self.assertEqual(expected, sql)


class TestExtractMainParts(TestSimpleSqlParser):
    def test_extract_main_parts_simple(self):
        select_clause, from_clause = simplesqlparser.extract_main_parts(self.sql_simple)
        expected = ('a,b', 'c')
        self.assertEqual(expected, (select_clause, from_clause))

    def test_extract_main_parts_simple_where(self):
        select_clause, from_clause = simplesqlparser.extract_main_parts(self.sql_simple_where)
        expected = ('a,b', 'c')
        self.assertEqual(expected, (select_clause, from_clause))

    def test_extract_main_parts_keyword_as_substring(self):
        select_clause, from_clause = simplesqlparser.extract_main_parts(self.sql_keyword_as_substring)
        expected = ('selector,b', 'c')
        self.assertEqual(expected, (select_clause, from_clause))


class TestExtractTableName(TestSimpleSqlParser):
    def test_extract_table_name_simple(self):
        table = simplesqlparser.extract_table_name(self.from_clause_simple)
        expected = 'c'
        self.assertEqual(expected, table)

    def test_extract_table_name_dbt(self):
        table = simplesqlparser.extract_table_name(self.from_clause_dbt)
        expected = 'c_2_g_coda_year_c'
        self.assertEqual(expected, table)


class TestExtractFieldName(TestSimpleSqlParser):
    def test_extract_filedname_simple(self):
        fieldname = simplesqlparser.extract_fieldname(self.fieldname_simple)
        expected = "bb"
        self.assertEqual(expected, fieldname)

    def test_extract_filedname_dbt(self):
        fieldname = simplesqlparser.extract_fieldname(self.fieldname_dbt)
        expected = "id"
        self.assertEqual(expected, fieldname)

    def test_extract_filedname_function(self):
        fieldname = simplesqlparser.extract_fieldname(self.fieldname_function)
        self.assertIsNone(fieldname)

    def test_extract_filedname_empty(self):
        fieldname = simplesqlparser.extract_fieldname(self.fieldname_empty)
        self.assertIsNone(fieldname)


class TestParse(TestSimpleSqlParser):
    def test_parse_sql_complex(self):
        table_data = simplesqlparser.parse(self.sql_complex)
        expected = {'table': 'c_2_g_coda_year_c',
                    'fields': ['id', 'name',
                               'c_2_g_status_c',
                               'c_2_g_start_date_c',
                               'c_2_g_end_date_c',
                               'c_2_g_owner_company_c'
                               ]
                    }
        self.assertDictEqual(expected, table_data)
