import os
import json

import simplesqlparser

SCRIPT_DIR = r'../sql_scripts'
TARGET_DIR = r'../table_desriptors'


def main():
    if not os.path.exists(TARGET_DIR):
        os.makedirs(TARGET_DIR)
    for entry in os.scandir(SCRIPT_DIR):
        if entry.path.endswith('.sql') and entry.is_file():
            with open(entry, 'r') as file:
                string = file.read()
                table_descriptor = simplesqlparser.parse(string)
                target_file = '{}{}{}.json'.format(TARGET_DIR, os.sep, table_descriptor['table'])
                with open(target_file, 'w') as outfile:
                    json.dump(table_descriptor, outfile, indent=4)


if __name__ == '__main__':
    main()
